#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <omp.h>

#define MAXCHARDOUBLE 64
#define MAXLINE 128
#define MAXFILENAME 64

/*stucture of a document*/
typedef struct document{
	int index; /*index of the document*/
	int cab_index; /*index of the cabinet where the document is*/
	double* subjects; /*subjects vector*/
} document_st;

/*structure of a cabinet*/
typedef struct cabinet{
	short int changed; /*flag that is 1 when a cabinet recieves or looses documents in an iteration*/
	double* average; /*average subjects vector*/
	double* deltaAverage; /*vector that takes into account the movements on the average of the cabinet for each iteration*/
	int deltaDocuments; /*Variation of the number of Documents of the cabinet for each iteration*/
	int numDocuments; /*number of documents in the cabinet*/
} cabinet_st;


/*****************************************************************************************************
* documentInit
*
* PURPOSE : Initialize a document structure
*
* RETURN :  document_st initialized or NULL pointer when error
*
* NOTES : Error handling must be done outside this function 
****************************************************************************************************/
document_st *documentInit(double* subjects, int numSubjects,int index){
	int k;
	document_st *document;
	
	if((document = (document_st*) malloc(sizeof(document_st))) == NULL)
		return NULL;
		
	if((document->subjects = (double*)malloc(numSubjects*sizeof(double)))==NULL)
		return NULL;
	
	for(k=0;k<numSubjects;k++){
		document->subjects[k] = subjects[k];
	}
	document->index = index;

	return document;
}

/****************************************************************************************************
* cabinetInit
*
* PURPOSE : Initialize a cabinet structure
*
* RETURN :  cabinet_st initialized or NULL pointer when error
*
* NOTES : Error handling must be done outside this function 
*****************************************************************************************************/
cabinet_st *cabinetInit(int numSubjects){
	int k;
	cabinet_st *cabinet;
	if((cabinet = (cabinet_st*) malloc(sizeof(cabinet_st))) == NULL)
		return NULL;
		
	if((cabinet->average = (double*)malloc(numSubjects*sizeof(double)))==NULL)
		return NULL;

	if((cabinet->deltaAverage = (double*)malloc(numSubjects*sizeof(double)))==NULL)
		return NULL;
				
	for(k=0;k<numSubjects;k++){
		cabinet->average[k] = 0;
		cabinet->deltaAverage[k] = 0;
	}
	cabinet->deltaDocuments = 0;
	cabinet->numDocuments = 0;
	cabinet->changed = 1;
	return cabinet;
}

/*****************************************************************************************************
* readfile
*
* PURPOSE : Read the input file and generate all the documents and respective cabinets.
*
* RETURN :  An array of cabinet_st with initial values required to begin the algorithm. Also retrieves the number of: cabinets, documents and subjects which pointers are passed as argument. Generates the DocumentMap which pointer is also passed as argument. 
*
* NOTES : Error handling is done inside this function. Always returns valid cabinet structure
*****************************************************************************************************/
cabinet_st** readfile(FILE* fp,document_st***documentMap, int *numCabinets,int *numDocuments,int *numSubjects, int argCabinets){
	char* lbuffer, nbuffer[MAXCHARDOUBLE];
	char cbuffer;
	int subjectcounter, l, k, i, docIndex, s;
	double *docSubjects;
	short int invalidDocument=0;
	cabinet_st** cabinetVector;
	document_st *document;
	
	
	/*Read first integer*/
	fscanf(fp,"%d",numCabinets);
	fgetc(fp);
	/*Read second integer*/
	fscanf(fp,"%d",numDocuments);
	fgetc(fp);
	/*Read third integer*/
	fscanf(fp,"%d",numSubjects);
	fgetc(fp);

	/*verificaton of correct values*/
	if(*numCabinets <= 0){
		printf("Number of Cabinets equal to or less than zero!\n");
		exit(-1);
	}
	if(*numDocuments <= 0){
		printf("Number of Documents equal to or less than zero!\n");
		exit(-1);
	}
	if(*numSubjects <= 0){
		printf("Number of Subjects equal to or less than zero!\n");
		exit(-1);
	}	
	
	/*if the number of cabinets was not suplied as an argument, retrieve this number from the file*/
	if(argCabinets != -1)
		*numCabinets = argCabinets;
	
	/*Allocate and init all the cabinets*/
	cabinetVector = (cabinet_st**)malloc(*numCabinets*sizeof(cabinet_st*));
	for(i=0;i<*numCabinets;i++){
		if((cabinetVector[i]=cabinetInit(*numSubjects))==NULL){
			printf("Memory allocation error\n");
			exit(-1);
		}
	}
	
	*documentMap = (document_st**)malloc(*numDocuments*sizeof(document_st*));
	
	lbuffer = (char*)malloc((2+*numSubjects*(MAXCHARDOUBLE+1))*sizeof(char));
	docSubjects= (double*)malloc(*numSubjects*sizeof(double));
	while(fgets(lbuffer,2+(*numSubjects)*(MAXCHARDOUBLE+1),fp) != NULL){ /*read a line*/
		/*Reset subjects index*/
		s=0;
		subjectcounter=0;
		/*Read document index*/	
		l=0;
		invalidDocument = 0;
		while(lbuffer[l] != ' '){
			if((lbuffer[l]>='0' && lbuffer[l]<='9')){
				nbuffer[l] = lbuffer[l];
				l++;
			}else{	
				invalidDocument = 1;
				break;/*invalid document line*/
			}
		}
		l++;
		nbuffer[l-1]='\n';
		docIndex=atoi(nbuffer);
		if(docIndex<0 || invalidDocument==1) continue; /*documento invalido, ignorado*/
		k=0;
		while(lbuffer[l]!= EOF && lbuffer[l]!= '\n'){
			cbuffer = lbuffer[l];
			if(k==0 && ((cbuffer>='0' && cbuffer<='9') || cbuffer=='-' || cbuffer=='.')){/*first number char*/
				nbuffer[k] = cbuffer;
				k++;
			}else if((cbuffer>='0' && cbuffer<='9') || cbuffer=='.' ){
				nbuffer[k] = cbuffer;
				k++;
			}else{
				nbuffer[k] = '\n';
				docSubjects[s] = atof(nbuffer);
				s++;
				subjectcounter++; /*contador que verifica se foram introduzidos o número correcto de subjects*/
				k=0;
			}
			l++;
		}
		if((lbuffer[l-1]>='0' && lbuffer[l-1]<='9')){
					nbuffer[k] = '\n';
					docSubjects[s] = atof(nbuffer);
					s++;
					subjectcounter++; /*contador que verifica se foram introduzidos o número correcto de subjects*/
		}
		if(subjectcounter == *numSubjects){
			/*Generate a document*/
			if((document = documentInit(docSubjects, *numSubjects, docIndex))==NULL){
				printf("Memory allocation error\n");
				exit(-1);
			}
			/*Insert document on the cabinet*/
			for(i=0;i<*numSubjects;i++)
				cabinetVector[docIndex%(*numCabinets)]->average[i]+=document->subjects[i];
			cabinetVector[docIndex%(*numCabinets)]->numDocuments++;
			(*documentMap)[docIndex] = document;
			(*documentMap)[docIndex]->cab_index = docIndex%(*numCabinets);
		}
	}
	for(k=0;k<*numCabinets;k++){
		if(cabinetVector[k]->numDocuments!=0){
			for(i=0;i<*numSubjects;i++)
				if(cabinetVector[k]->numDocuments!=0)				
					cabinetVector[k]->average[i]/=cabinetVector[k]->numDocuments;
		}
	}
	free(lbuffer);
	free(docSubjects);
	return cabinetVector;
}

/*****************************************************************************************************
* compute_averages
*
* PURPOSE : Compute the averages for each cabinet if required 
*
* RETURN :  cabinet_st with updated values of averages
*
* NOTES : Always returns valid cabinet structure
*****************************************************************************************************/
cabinet_st** compute_averages(cabinet_st **cabinetVector,document_st **documentMap, int numSubjects, int numCabinets, int numDocuments){
	int i,j;
	
	for(i=0;i<numCabinets;i++){ 	
		if(cabinetVector[i]->changed){
			if((cabinetVector[i]->numDocuments + cabinetVector[i]->deltaDocuments)!=0){
				for(j=0;j<numSubjects;j++){				
					cabinetVector[i]->average[j] = ((cabinetVector[i]->average[j])*cabinetVector[i]->numDocuments+cabinetVector[i]->deltaAverage[j])/(cabinetVector[i]->numDocuments+cabinetVector[i]->deltaDocuments);
					cabinetVector[i]->deltaAverage[j]=0;
				}
			}else{
				for(j=0;j<numSubjects;j++)
					cabinetVector[i]->average[j] = 0;
					cabinetVector[i]->deltaAverage[j]=0;
			}
			cabinetVector[i]->numDocuments += cabinetVector[i]->deltaDocuments;
			cabinetVector[i]->deltaDocuments = 0;
			cabinetVector[i]->changed = 0;
		}
	}
	
	return cabinetVector;
}

/*****************************************************************************************************
* desired_cabinet
*
* PURPOSE : Determine the index of the desired cabinet which minimizes the distance from a certain document to its mean
*
* RETURN : index of the desired cabinet
*
* NOTES : 
*****************************************************************************************************/
int desired_cabinet(document_st *document, cabinet_st **cabinetVector, int numCabinets, int numSubjects){
	int j, k, indexmin = -1;
	double dist=0, distmin=0;
	
	for(k=0;k<numCabinets;k++){ /*verifica qual a cabinet que minimiza a distancia ao doc*/
		dist=0;
		for(j=0;j<numSubjects;j++)
			dist += pow(cabinetVector[k]->average[j]-(document->subjects[j]),2);
		if(dist<distmin || indexmin ==-1){
			distmin=dist;
			indexmin = k;
		}
	}
	return indexmin;
}

/*****************************************************************************************************
* writefile
*
* PURPOSE : Create and fill the output file
*
* RETURN : 0 on success or -1 otherwise
*
* NOTES : Error handling must be done outside this function
*****************************************************************************************************/
int writefile(char* infilename,document_st **documentMap,int numDocuments,int numSubjects){
	
	int i = 0;
	char outfilename[MAXFILENAME];	
	FILE * fp_out;

	if(strlen(infilename)-3 < 0 || strlen(infilename) > MAXFILENAME) return -1;

	for(i=0;i<(strlen(infilename)-3);i++){
		outfilename[i] = infilename[i];
	}
	outfilename[strlen(infilename)-3] = '\0';

	if(strcat(outfilename,".out")==NULL) return -1;

	fp_out = fopen(outfilename,"w");

	if(fp_out==NULL) return -1;

	for(i=0;i<numDocuments;i++)
		fprintf(fp_out,"%d %d\n",i,documentMap[i]->cab_index);
	
	fclose(fp_out);
	return 0;
}

int main(int argc, char** argv){
	int argCabinets = -1, numCabinets, numSubjects, numDocuments;
	int i,j;
  double start, end, algstart, algend;
	int stable=0;
	int indexmin;
	document_st **documentMap;
	cabinet_st** cabinetVector;
	
	FILE* fp;

	start = omp_get_wtime(); /*obtain the program's start time*/
	
	/*if no filename is passed by argument shows the correct use of the program*/
	if(argc<2){
		printf("Usage: ./docs-serial <file_name> [n of cabinets]\n");
		exit(-1);
	}
	else if(argc>2){
		/*argv[1] contains de input filename*/
		if(argc>=2 && (argCabinets = atoi(argv[2])) <= 0){
			printf("Usage: ./docs-serial <file_name> [n of cabinets]\n");
			printf("Usage: n of cabinets must be a positive integer\n");
			exit(-1);
		}
	}
	if((fp = fopen(argv[1],"r"))==NULL){
		printf("File not found\n");
		exit(-1);
	}
	
	cabinetVector = readfile(fp,&documentMap, &numCabinets, &numDocuments, &numSubjects, argCabinets);
	
	fclose(fp);
	
/*** PROGRAM MAIN CYCLE ***/
	algstart = omp_get_wtime(); /*obtain the program's start time*/
	
	while(1){

		stable=1;
		for(i=0;i<numDocuments;i++){
			indexmin = desired_cabinet(documentMap[i], cabinetVector, numCabinets, numSubjects); /*Choose desired cabinet*/

			if (indexmin != documentMap[i]->cab_index) { /* Move document to desired cabinet*/
				stable=0;			
				for(j=0;j<numSubjects;j++){
					cabinetVector[documentMap[i]->cab_index]->deltaAverage[j]-=documentMap[i]->subjects[j];
					cabinetVector[indexmin]->deltaAverage[j]+=documentMap[i]->subjects[j];
				}
				cabinetVector[documentMap[i]->cab_index]->changed = 1; /*Set the destination and source cabinets as changed*/
				cabinetVector[indexmin]->changed = 1;
				cabinetVector[documentMap[i]->cab_index]->deltaDocuments--;	/*Update the number of documents in the destination and source cabinets*/
				cabinetVector[indexmin]->deltaDocuments++;	
				documentMap[i]->cab_index = indexmin;	/*Place the document in the new cabinet*/

			}
		}
		
		if(stable==1)
			break;
			
		cabinetVector = compute_averages(cabinetVector,documentMap, numSubjects, numCabinets, numDocuments); /*Compute averages*/		
	}
	
	algend = omp_get_wtime(); /*obtain the program's start time*/
	/*Print output file*/
	if(writefile(argv[1],documentMap,numDocuments,numSubjects) == -1){
		printf("Error writing output file\n");
		exit(-1);	
	}

	/*Free allocated memory*/
	for(i=0;i<numCabinets;i++){
		free(cabinetVector[i]->average);
		free(cabinetVector[i]);
	}
	free(cabinetVector);
	for(i=0;i<numDocuments;i++){
		free(documentMap[i]->subjects);
		free(documentMap[i]);
	}
	free(documentMap);

	end = omp_get_wtime();  /*obtain the program's finish time*/

	printf("FullTime: %f\nAlgorithmTime: %f\n", end-start, algend-algstart);

	return 0;
}
