#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <mpi.h>

#define MAXCHARDOUBLE 64
#define MAXLINE 128
#define MAXFILENAME 64
#define WTAG 1
#define KILLTAG 9

int handleArguments(int argc,char **argv, int* argCabinets);
void readParameters(FILE* fp,double **cabinetVector, double **deltaVector, double **rcvbuf, int *combuf, int argCabinets);
double* readfile(FILE* fp, double** cabinetVector, int numCabinets,int numDocuments,int numSubjects,int np);
int desired_cabinet(int* combuf,int blocksize,double* cabinetVector,double* documentBlock,int docindex);
FILE* createfile(char* infilename);

/***********************************************************************
* handleArguments
*
* PURPOSE : Validates the arguments introduced by the user
*
* RETURN :  0 on success or -1 otherwise
*
* NOTES : Error handling must be done outside this function 
************************************************************************/
int handleArguments(int argc,char **argv, int* argCabinets){
	if(argc<2){
		printf("Usage: ./docs-serial <file_name> [n of cabinets]\n");
		return(-1);
	}else if(argc>2){
		/*argv[1] contains de input filename*/
		if(argc>=2 && (*argCabinets = atoi(argv[2])) <= 0){
			printf("Usage: ./docs-mpi <file_name> [n of cabinets]\n");
			printf("Usage: n of cabinets must be a positive integer\n");
			return(-1);
		}
	}
	return 0;
}

/***********************************************************************
* readParamenters
*
* PURPOSE : Read from the input file the number of Cabinets,Subjects and Documents
*
* RETURN :
*
* NOTES :
************************************************************************/
void readParameters(FILE* fp,double **cabinetVector, double **deltaVector, double **rcvbuf, int *combuf, int argCabinets){
	int i;
	
	fscanf(fp,"%d",&combuf[0]);
	fgetc(fp);
	fscanf(fp,"%d",&combuf[1]);
	fgetc(fp);
	fscanf(fp,"%d",&combuf[2]);
	fgetc(fp);

	/*verificaton of correct values*/
	if(combuf[0] <= 0){
		printf("Number of Cabinets equal to or less than zero!\n");
		exit(-1);
	}
	if(combuf[1] <= 0){
		printf("Number of Documents equal to or less than zero!\n");
		exit(-1);
	}
	if(combuf[2] <= 0){
		printf("Number of Subjects equal to or less than zero!\n");
		exit(-1);
	}	
	if(argCabinets != -1)
		combuf[0] = argCabinets;
		
	(*cabinetVector) = (double*)malloc(combuf[0]*(combuf[2]+2)*sizeof(double));
	(*deltaVector) = (double*)malloc(combuf[0]*(combuf[2]+2)*sizeof(double));
	(*rcvbuf) = (double*)malloc(combuf[0]*(combuf[2]+2)*sizeof(double));
	for(i=0;i<combuf[0]*(combuf[2]+2);i++)
		(*cabinetVector)[i]=(*deltaVector)[i]=(*rcvbuf)[i]=0;
	
}

/*****************************************************************************************************
* readfile
*
* PURPOSE : Read the input file and generate, initialize and send to other processes all the data structures required by the algorithm.
*
* RETURN :  An array of doubles with the information about the documents assigned to the main process.
*
* NOTES : Error handling is done inside this function. Always returns valid cabinet structure
****************************************************************************************************/
double* readfile(FILE* fp, double** cabinetVector, int numCabinets,int numDocuments,int numSubjects,int np){
	char* lbuffer, nbuffer[MAXCHARDOUBLE];
	char cbuffer;
	int subjectcounter, l, k, i, docIndex, s,wait_en=0;
	double *docSubjects;
	double numdocs;
	short int invalidDocument=0;
	int maxNumDocs,minNumDocs,dcounter=0,npcounter=1;
	MPI_Request sendRequest;
 	double* documentBlock;
 	double* documentBlock_next;
 	double* documentBlock_aux;
	
	lbuffer = (char*)malloc((2+numSubjects*(MAXCHARDOUBLE+1))*sizeof(char));
	docSubjects= (double*)malloc(numSubjects*sizeof(double));
	
	maxNumDocs	= ceil((double)numDocuments/(double)np);
	minNumDocs  = numDocuments/np;
	documentBlock = (double*)malloc(maxNumDocs*(numSubjects+2)*sizeof(double));
	documentBlock_next = (double*)malloc(maxNumDocs*(numSubjects+2)*sizeof(double));
	while(fgets(lbuffer,2+numSubjects*(MAXCHARDOUBLE+1),fp) != NULL){ /*read a line*/
		/*Reset subjects index*/
		s=0;
		subjectcounter=0;
		/*Read document index*/	
		l=0;
		invalidDocument = 0;
		while(lbuffer[l] != ' '){
			if((lbuffer[l]>='0' && lbuffer[l]<='9')){
				nbuffer[l] = lbuffer[l];
				l++;
			}else{	
				invalidDocument = 1;
				break;/*invalid document line*/
			}
		}
		l++;
		nbuffer[l-1]='\n';
		docIndex=atoi(nbuffer);
		if(docIndex<0 || invalidDocument==1) continue; /*Invalid document, ignored*/
		k=0;
		while(lbuffer[l]!= EOF && lbuffer[l]!= '\n'){
			cbuffer = lbuffer[l];
			if(k==0 && ((cbuffer>='0' && cbuffer<='9')  || cbuffer=='-' || cbuffer=='.')){
				nbuffer[k] = cbuffer;
				k++;
			}else if((cbuffer>='0' && cbuffer<='9') || cbuffer=='.' ){
				nbuffer[k] = cbuffer;
				k++;
			}else{
				nbuffer[k] = '\n';
				docSubjects[s] = atof(nbuffer);
				s++;
				subjectcounter++; /*Counter that verify if the correct number of subjects was introduced*/
				k=0;
			}
			l++;
		}
		if((lbuffer[l-1]>='0' && lbuffer[l-1]<='9')){
			nbuffer[k] = '\n';
			docSubjects[s] = atof(nbuffer);
			s++;
			subjectcounter++; /*Counter that verify if the correct number of subjects was introduced*/
		}
		if(subjectcounter == numSubjects){	
			/*Generate and assign the document to a cabinet*/
			for(i=0; i<numSubjects; i++){
				documentBlock[dcounter*(numSubjects+2)+2+i]=docSubjects[i];
				(*cabinetVector)[docIndex%(numCabinets)*(numSubjects+2)+2+i]+=docSubjects[i]; /*Update the sum for each subject with the new document*/
			}
			documentBlock[dcounter*(numSubjects+2)]=docIndex; /*Updates document index*/
			documentBlock[dcounter*(numSubjects+2)+1]=docIndex%numCabinets; /*Updates index of the cabinet where the document was assigned*/
			(*cabinetVector)[(docIndex%numCabinets)*(numSubjects+2)+1]++; /*Increase the number of documents in the respective cabinet*/

			if(npcounter < numDocuments%np && dcounter==(maxNumDocs-1) && npcounter!=np){/*big chunk*/
				/*wait for previous send to finish*/
				if(wait_en)MPI_Wait(&sendRequest,MPI_STATUS_IGNORE);
				/*switch pointers and send*/
				documentBlock_aux=documentBlock;
				documentBlock=documentBlock_next;
				documentBlock_next=documentBlock_aux;
				MPI_Isend(documentBlock_next, maxNumDocs*(numSubjects+2), MPI_DOUBLE, npcounter, WTAG, MPI_COMM_WORLD,&sendRequest);
				npcounter++;
				dcounter=0;
				wait_en=1;
			}else if(npcounter >= numDocuments%np && dcounter==(minNumDocs-1) && npcounter!=np){/*small chunk*/
				/*wait for previous send to finish*/
				if(wait_en)MPI_Wait(&sendRequest,MPI_STATUS_IGNORE);
				/*switch pointers and send*/
				documentBlock_aux=documentBlock;
				documentBlock=documentBlock_next;
				documentBlock_next=documentBlock_aux;
				MPI_Isend(documentBlock_next, minNumDocs*(numSubjects+2), MPI_DOUBLE, npcounter, WTAG, MPI_COMM_WORLD,&sendRequest);
				npcounter++;
				dcounter=0;
				wait_en=1;
			}else{
				dcounter++;
			}
		}
	}

	if(npcounter<np){
	  for(i=npcounter;i<np;i++){/*Kill unemployed threads*/
	    MPI_Send(documentBlock, minNumDocs*(numSubjects+2), MPI_DOUBLE, i, KILLTAG, MPI_COMM_WORLD);
	  }
	}
	
	for(i=0;i<numCabinets*(numSubjects+2);i++){
		if(i%(numSubjects+2)==1)
			numdocs=(*cabinetVector)[i];
		else if(i%(numSubjects+2)!=0){
			if(numdocs!=0)(*cabinetVector)[i]/=numdocs;
		}
	}
	free(lbuffer);
	free(docSubjects);
	if(wait_en)MPI_Wait(&sendRequest,MPI_STATUS_IGNORE);/*wait for the last send to free*/
	free(documentBlock_next);
	return documentBlock;
}


int main(int argc, char** argv){
	int id, np;
	double secs,algsecs;
	int combuf[3];
	int i, j, argCabinets = -1;
	double* cabinetVector;
	double* deltaVector;
	double* rcvbuf;
	double* documentBlock;
	int* documentResult;
	int desiredcab, currentcab, oldnumdocs, newnumdocs;
	int maxNumDocs,minNumDocs;
	short int terminated;
  	int blocksize;
  	MPI_Status recvStatus;
	
	FILE* fp;
	FILE* fp_out;
	
	if(handleArguments(argc,argv, &argCabinets)==-1)
		exit(-1);
	
	MPI_Init (&argc, &argv);

	MPI_Comm_rank (MPI_COMM_WORLD, &id);
	MPI_Comm_size (MPI_COMM_WORLD, &np);
	MPI_Barrier(MPI_COMM_WORLD);
	secs = - MPI_Wtime();
  
	if(id==0){
		if((fp = fopen(argv[1],"r"))==NULL){
			printf("File not found\n");
			exit(-1);
		}
		readParameters(fp, &cabinetVector, &deltaVector, &rcvbuf, combuf, argCabinets);
	}
  	MPI_Bcast(combuf,3,MPI_INT,0,MPI_COMM_WORLD);/*Give the number of cabinets/documents/subjects to all processes*/
	
	/*Compute document sending and distribution variables*/	
	maxNumDocs	= ceil((double)combuf[1]/(double)np);
	minNumDocs  = combuf[1]/np;
	
	if(id==0){
		documentBlock = readfile(fp, &cabinetVector, combuf[0], combuf[1], combuf[2],np);
		blocksize = maxNumDocs*(combuf[2]+2);
	  documentResult = (int*)malloc(maxNumDocs*2*sizeof(int));
	}else{
		cabinetVector = (double*)malloc(combuf[0]*(combuf[2]+2)*sizeof(double));
	  deltaVector = (double*)malloc(combuf[0]*(combuf[2]+2)*sizeof(double));
	  rcvbuf = (double*)malloc(combuf[0]*(combuf[2]+2)*sizeof(double));
		for(i=0;i<combuf[0]*(combuf[2]+2);i++)
			deltaVector[i]=rcvbuf[i]=0;
			
		if(id < combuf[1]%np)/*Thread is interested in recieving a big document chunk*/
		  blocksize = maxNumDocs*(combuf[2]+2);
    else/*Thread is interested in recieving a small document chunk*/
      blocksize = minNumDocs*(combuf[2]+2);
      
    documentBlock = (double*)malloc(blocksize*sizeof(double));
    documentResult = (int*)malloc((blocksize/(combuf[2]+2))*2*sizeof(int));
    MPI_Recv(documentBlock,blocksize,MPI_DOUBLE,0,MPI_ANY_TAG,MPI_COMM_WORLD,&recvStatus);
    
    if(recvStatus.MPI_TAG==KILLTAG)
			printf("Warning: The process with id=%d will be idle the entire execution time\n",id);
	}
	
	MPI_Bcast(cabinetVector,combuf[0]*(combuf[2]+2),MPI_DOUBLE,0,MPI_COMM_WORLD);/*Distributes de information about the initial status of all cabinets*/
	algsecs = - MPI_Wtime();
  while(1){/*Work cycle*/

    for(i=0;i<blocksize/(combuf[2]+2);i++){/*For all my documents*/   
      currentcab = (int)documentBlock[i*(combuf[2]+2)+1];  
      desiredcab = desired_cabinet(combuf,blocksize,cabinetVector,documentBlock,i);

      if(desiredcab != currentcab){/*If we want to move the document*/
        deltaVector[currentcab*(combuf[2]+2)] = 1;/*Flag changed++ of the source cabinet*/
        for(j=0;j<combuf[2];j++)/*Remove the doc subj contribute to the mean*/
          deltaVector[currentcab*(combuf[2]+2)+2+j]-=documentBlock[i*(combuf[2]+2)+2+j];
        deltaVector[currentcab*(combuf[2]+2)+1]--;/*Decrease deltaDocs of the source cabinet*/    
        deltaVector[desiredcab*(combuf[2]+2)] = 1;/*Flag changed++ of the destination cabinet*/
        for(j=0;j<combuf[2];j++)/*Add the doc subj contribute to the mean*/
          deltaVector[desiredcab*(combuf[2]+2)+2+j]+=documentBlock[i*(combuf[2]+2)+2+j];
        deltaVector[desiredcab*(combuf[2]+2)+1]++;/*Increase deltaDocs of the destination cabinet*/
        documentBlock[i*(combuf[2]+2)+1] = desiredcab;/*Update de cabinet where the document is assigned*/
        
      }
    }
	
    /*at this point, the deltaVector of this thread is ready to be Reduced.*/

    MPI_Allreduce(deltaVector,rcvbuf,combuf[0]*(combuf[2]+2),MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);  /*Sum the alterations to all cabinets from all processes*/

    terminated = 1;
    for(i=0;i<combuf[0]*(combuf[2]+2);i++){
    	if(i%(combuf[2]+2)==0){
    		if(rcvbuf[i]==0){ /*If the cabinet has not been changed*/
    			i+=(combuf[2]+2)-1; /*Goes to the next cabinet*/
    			continue;
    		}
    	}else if(i%(combuf[2]+2)==1){
    		terminated = 0;
    		oldnumdocs=cabinetVector[i];
    		cabinetVector[i]+=rcvbuf[i];
    		newnumdocs=cabinetVector[i];
    	}else{
    		if(newnumdocs!=0)cabinetVector[i]=(cabinetVector[i]*oldnumdocs + rcvbuf[i])/newnumdocs; /*Update the mean of the cabinet*/
    	}
    	deltaVector[i]=0;
    	rcvbuf[i]=0;
    }
    if(terminated)
    	break;
    	
  }

  /*Pack results*/
  for(i=0;i<blocksize/(combuf[2]+2);i++){/*Compute the root results*/
    documentResult[2*i] = (int)documentBlock[i*(combuf[2]+2)]; 
    documentResult[2*i+1] = (int)documentBlock[i*(combuf[2]+2)+1];
  }  
  
  if(id==0){
  
    if((fp_out = createfile(argv[1]))==NULL){
      printf("failed to create output file\n");
      exit(-1); 
    }
   
    for(i=1;i<np;i++){
      if(i < combuf[1]%np){ /*Thread is interested in sending a big document chunk*/
        MPI_Recv(documentResult,maxNumDocs*(combuf[2]+2),MPI_INT,i,i,MPI_COMM_WORLD,&recvStatus);
        for(j=0;j<maxNumDocs;j++)
          fprintf(fp_out,"%d %d\n",documentResult[2*j],documentResult[2*j+1]);
              
      }else{ /*Thread is interested in send a small document chunk*/
        MPI_Recv(documentResult,minNumDocs*(combuf[2]+2),MPI_INT,i,i,MPI_COMM_WORLD,&recvStatus);
        for(j=0;j<minNumDocs;j++)
          fprintf(fp_out,"%d %d\n",documentResult[2*j],documentResult[2*j+1]);
      }
    }
    for(i=0;i<blocksize/(combuf[2]+2);i++){/*Compute the root results*/
      documentResult[2*i] = (int)documentBlock[i*(combuf[2]+2)]; 
      documentResult[2*i+1] = (int)documentBlock[i*(combuf[2]+2)+1];
    }  
    for(i=0;i<blocksize/(combuf[2]+2);i++)
      fprintf(fp_out,"%d %d\n",documentResult[2*i],documentResult[2*i+1]);
      
  }else{
    MPI_Send(documentResult,2*(blocksize/(combuf[2]+2)),MPI_INT,0,id,MPI_COMM_WORLD);/*Send the result block to the master thread with id as tag*/
  }
  
	free(documentBlock);
  free(cabinetVector);
  free(deltaVector);
  free(documentResult);
  free(rcvbuf);
  
	if(id==0){
		fclose(fp);
		fclose(fp_out);
	}

  MPI_Barrier(MPI_COMM_WORLD);
	secs += MPI_Wtime();
	algsecs += MPI_Wtime();
	if(id==0)printf("Fulltime:%f\nAlgorithmTime:%f\n",secs,algsecs);
  MPI_Finalize();

	return 0;
}

/*****************************************************************************************************
* desired_cabinet
*
* PURPOSE : Determine the index of the desired cabinet which minimizes the distance from a certain document to its mean
*
* RETURN : index of the desired cabinet
*
* NOTES : 
****************************************************************************************************/
int desired_cabinet(int* combuf,int blocksize,double* cabinetVector,double* documentBlock,int docindex){
  int j,k;
	double dist=0, distmin=0, indexmin=-1;
	
  for(j=0;j<combuf[0];j++){/*for all cabinets*/
    dist=0;
    for(k=0;k<combuf[2];k++)/*for all subject coordinates*/
	    dist += pow(cabinetVector[j*(combuf[2]+2)+2+k]-documentBlock[docindex*(combuf[2]+2)+2+k],2);
	  if(dist<distmin || indexmin ==-1){
	    distmin = dist;/*store the new minimum distance*/
	    indexmin = j;/*collect the cabinet index*/
    }
  }
  return indexmin;
}

/*****************************************************************************************************
* writefile
*
* PURPOSE : Create and fill the output file
*
* RETURN : 0 on success or -1 otherwise
*
* NOTES : Error handling must be done outside this function
****************************************************************************************************/
FILE* createfile(char* infilename){
	int i = 0;
	char outfilename[MAXFILENAME];	
	FILE * fp_out;

	if(strlen(infilename)-3 < 0 || strlen(infilename) > MAXFILENAME) return NULL;

	for(i=0;i<(strlen(infilename)-3);i++){
		outfilename[i] = infilename[i];
	}
	outfilename[strlen(infilename)-3] = '\0';

	if(strcat(outfilename,".out")==NULL) return NULL;

	fp_out = fopen(outfilename,"w");
	
	return fp_out;
}

